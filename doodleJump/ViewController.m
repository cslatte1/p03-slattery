//
//  ViewController.m
//  doodleJump
//
//  Created by Ciaran Slattery on 2/19/17.
//  Copyright © 2017 Ciaran Slattery. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

-(IBAction)jumpButtonMove:(id)sender{
    [self hitsTopBound];
    jumpVal = 40;
    
}


-(IBAction)startLeft:(id)sender{
    
    [self hitsLeftBound];
    
    leftTimer = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(leftMove) userInfo:nil repeats:YES];
    
}
-(IBAction)endLeft:(id)sender{
    [leftTimer invalidate];
    
}
-(IBAction)startRight:(id)sender{
    
    [self hitsRightBound];
    
    rightTimer = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(rightMove) userInfo:nil repeats:YES];

}
-(IBAction)endRight:(id)sender{
    
    [rightTimer invalidate];
}


- (void) leftMove{
    
    mario.center= CGPointMake(mario.center.x-12, mario.center.y);
    [self hitsLeftBound];
}


- (void) rightMove{
    
    mario.center= CGPointMake(mario.center.x+12, mario.center.y);
    [self hitsRightBound];
}



-(void) fallingAction{
    jumpVal = jumpVal - 5;
    mario.center = CGPointMake(mario.center.x, mario.center.y-jumpVal);
    
    if(CGRectIntersectsRect(mario.frame, floor.frame)){
        jumpVal = 0;
        mario.center = CGPointMake(mario.center.x, floor.center.y - 38);
    }
    
    if(CGRectIntersectsRect(mario.frame, platform1.frame)){
        jumpVal = 0;
        mario.center = CGPointMake(mario.center.x, platform1.center.y - 38);
    }
    
    if(CGRectIntersectsRect(mario.frame, platform2.frame)){
        jumpVal = 0;
        mario.center = CGPointMake(mario.center.x, platform2.center.y - 38);
    }
    
    if(CGRectIntersectsRect(mario.frame, platform3.frame)){
        jumpVal = 0;
        mario.center = CGPointMake(mario.center.x, platform3.center.y - 38);
    }

    if(CGRectIntersectsRect(mario.frame, platform4.frame)){
        jumpVal = 0;
        mario.center = CGPointMake(mario.center.x, platform4.center.y - 38);
    }

    if(CGRectIntersectsRect(mario.frame, platform5.frame)){
        jumpVal = 0;
        mario.center = CGPointMake(mario.center.x, platform5.center.y - 38);
    }
    
    if(CGRectIntersectsRect(mario.frame, platform6.frame)){
        jumpVal = 0;
        mario.center = CGPointMake(mario.center.x, platform6.center.y - 38);
    }
    
    if(CGRectIntersectsRect(mario.frame, platform7.frame)){
        jumpVal = 0;
        mario.center = CGPointMake(mario.center.x, platform7.center.y - 38);
    }

    
    if(CGRectIntersectsRect(mario.frame, topBoundary.frame)){
        mario.center = CGPointMake(mario.center.x, bottomBoundary.center.y);
    }
    
    if(CGRectIntersectsRect(mario.frame, bottomBoundary.frame)){
        jumpVal = 0;
        mario.center = CGPointMake(mario.center.x, bottomBoundary.center.y - 38);
    }
}

-(void)hitsLeftBound{
    if(CGRectIntersectsRect(mario.frame, leftBoundary.frame)){
        mario.center = CGPointMake(rightBoundary.center.x, mario.center.y);
    }
}

-(void)hitsRightBound{
    if(CGRectIntersectsRect(mario.frame, rightBoundary.frame)){
        mario.center = CGPointMake(leftBoundary.center.x, mario.center.y);
    }
}

-(void)hitsTopBound{
    if(CGRectIntersectsRect(mario.frame, topBoundary.frame)){
        mario.center = CGPointMake(mario.center.x, bottomBoundary.center.y);
    }
}


- (void)viewDidLoad {
    
    fallingTimer = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(fallingAction) userInfo:nil repeats:YES];
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"marioWallpaper.jpg"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
