//
//  ViewController.h
//  doodleJump
//
//  Created by Ciaran Slattery on 2/19/17.
//  Copyright © 2017 Ciaran Slattery. All rights reserved.
//

#import <UIKit/UIKit.h>

int jumpVal;

@interface ViewController : UIViewController

{
    IBOutlet UIImageView *mario;
    IBOutlet UIImageView *floor;
    IBOutlet UIImageView *platform1;
    IBOutlet UIImageView *platform2;
    IBOutlet UIImageView *platform3;
    IBOutlet UIImageView *platform4;
    IBOutlet UIImageView *platform5;
    IBOutlet UIImageView *platform6;
    IBOutlet UIImageView *platform7;
    IBOutlet UIImageView *leftBoundary;
    IBOutlet UIImageView *rightBoundary;
    IBOutlet UIImageView *topBoundary;
    IBOutlet UIImageView *bottomBoundary;
    IBOutlet UIButton *jumpButton;
    IBOutlet UIButton *leftButton;
    IBOutlet UIButton *rightButton;

    NSTimer *fallingTimer;
    NSTimer *leftTimer;
    NSTimer *rightTimer;
    
}

- (IBAction)jumpButtonMove:(id)sender;
- (void) fallingAction;
- (void) leftMove;
- (void) rightMove;

-(IBAction)startLeft:(id)sender;
-(IBAction)endLeft:(id)sender;
-(IBAction)startRight:(id)sender;
-(IBAction)endRight:(id)sender;

-(void)hitsLeftBound;
-(void)hitsRightBound;
-(void)hitsTopBound;
//-(void)hitsBottomBound;

@end

