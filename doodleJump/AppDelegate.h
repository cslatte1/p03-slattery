//
//  AppDelegate.h
//  doodleJump
//
//  Created by Ciaran Slattery on 2/19/17.
//  Copyright © 2017 Ciaran Slattery. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

