# README #

### Mario World Doodle Jump (Project 3) ###

* Platform jumper game that utilizes three buttons: "Left", "Right", and "Jump!". When the player gets Mario to jump to the top, the game resets and he starts back on the ground again. Additionally, the player can reach far-reaching platforms by jumping off screen to one side and ending up on the other side.

### Quirks ###

* The player should only click on the "Jump!" button when their y-velocity has reached zero. Otherwise, they could "button mash" Mario to the top and avoid all of the platforms.

* As long as Mario touches a platform, he will be transported to the top, making the game easier for the player.




* Ciaran Slattery, CS 441, Binghamton University